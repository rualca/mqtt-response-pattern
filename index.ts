import { Requester } from './src/Requester';
import { RequesterBasic } from './src/RequesterBasic';
import { SubscriberBasic } from './src/SubscriberBasic';
import { MqttMainClient } from './src/utils/MqttMainClient';

const mqttURL = 'mqtt:/127.0.0.1';
const topic = 'request/+/+'
const mqttMainClient = MqttMainClient.init(mqttURL);

// RequesterBasic.init(mqttMainClient);
SubscriberBasic.init(mqttMainClient, topic);

Requester.init(mqttMainClient, ['response/+/+', 'otherTopics/#']);
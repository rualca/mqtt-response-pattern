import EventEmitter from "events";
import { IClientPublishOptions, MqttClient } from "mqtt";
import { publishWithResponse } from "./helpers/mqtt-async.helper";
import { relayResponseEvent } from "./helpers/relayResponseEvent.helper";

export class Requester {
  private readonly eventEmitter = new EventEmitter();
  private readonly mqttClient: MqttClient;
  private topics: string[];

  private constructor(mqttClient: MqttClient, topics: string[]) {
    this.mqttClient = mqttClient
    this.topics = topics
  }

  public static init(mqttClient: MqttClient, topics: string[]) {
    const instance = new this(mqttClient, topics)

    topics.forEach((topic) => instance.mqttClient.subscribe(topic))

    instance.mqttClient.on("message", (topic, payload) => {
      const [mainTopic, deviceName, relayName] = topic.split("/"); //spliting the topic ==> [response,deviceName,relayName]
      
      switch (mainTopic) {
        case "response":
          return relayResponseEvent({
            eventEmitter: instance.eventEmitter,
            deviceName,
            relayName,
            payload,
          });
        case "otherTopics":
          console.log("other topics");
          return;
        default:
          return console.log("can not find anything");
      }
    });

    setTimeout(() => {
      instance.startSystem();
    }, 1000);

    return instance;
  }

  async startSystem() {
    await this.startResponsePatternExample({
      deviceName: "device_1",
      relayName: "relay_1",
      message: 1,
    });
    await this.startResponsePatternExample({
      deviceName: "device_2",
      relayName: "relay_1",
      message: 1,
    });
  };
  
  async startResponsePatternExample({
    deviceName,
    relayName,
    message,
  }: {
    deviceName: string;
    relayName: string;
    message: 1 | 0;
  }) {
    try {
      const responseTopic = `response/${deviceName}/${relayName}`;
      const requestTopic = `request/${deviceName}/${relayName}`;
      const responseEventName = `responseEvent/${deviceName}/${relayName}`;
  
      const publishOptions: IClientPublishOptions = {
        qos: 1,
        properties: {
          responseTopic,
          correlationData: Buffer.from("secret", "utf-8"),
        },
      };
  
      const responseMessage = await publishWithResponse({
        client: this.mqttClient,
        data: message,
        publishOptions,
        requestTopic,
        responseEventName,
        eventEmitter: this.eventEmitter,
      });
  
      console.log(`${deviceName}/${relayName} : ${responseMessage.message}`);
    } catch (error) {
      console.log(`${deviceName}/${relayName} : ${error}`);
    }
  };
}

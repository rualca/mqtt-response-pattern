import mqtt, { IClientOptions, MqttClient } from 'mqtt';

export class MqttMainClient {
  private readonly options: IClientOptions = {
    port: 1883,
    protocolVersion: 5,
    keepalive: 60,
    properties: {
      requestResponseInformation: true,
      requestProblemInformation: true,
    },
  };

  private mqttClient: MqttClient

  private constructor(connectionURL: string) {
    this.mqttClient = mqtt.connect(connectionURL, this.options);
  }

  public static init(mqttConnectionURL: string) {
    const instance = new this(mqttConnectionURL)

    instance.mqttClient.on('connect', () => {
      console.log(`connected to mqtt  ${new Date()}`);
    });
    
    instance.mqttClient.on('error', (err) => {
      console.log(err);
    });

    return instance.mqttClient;
  }
}
import { IClientPublishOptions, MqttClient } from 'mqtt';
import { MqttMainClient } from './utils/MqttMainClient';
import { publishWithResponseBasic } from './helpers/mqtt-async.helper';

export class RequesterBasic {
  private readonly deviceName = 'device_1';
  private readonly relayName = 'relay_1';
  private mqttClient: MqttClient

  constructor(mqttClient: MqttClient) {
    this.mqttClient = mqttClient
  }

  public static init(mqttClient: MqttClient) {
    console.log('Starting Requester Basic...');
    
    const instance = new this(mqttClient)

    setTimeout(() => {
      startSystem();
    }, 1000);
    
    const startSystem = () => {
      instance.startResponsePatternExample();
    };

    return instance;
  }
  
  async startResponsePatternExample() {
    const responseTopic = `response/${this.deviceName}/${this.relayName}`;
    const requestTopic = `request/${this.deviceName}/${this.relayName}`;
    const publishOptions: IClientPublishOptions = {
      qos: 1,
      properties: {
        responseTopic,
        correlationData: Buffer.from('secret', 'utf-8'),
      },
    };
  
    try {
      const responseMessage = await publishWithResponseBasic({
        client: MqttMainClient.init('mqtt:/127.0.0.1'),
        publishOptions,
        requestTopic,
        responseTopic,
        message: 1,
      });
      
      console.log({responseMessage});
    } catch (error) {
      console.log({error});
    }
  };
}

import { IClientSubscribeOptions, MqttClient } from 'mqtt';

export class SubscriberBasic {
  private readonly options: IClientSubscribeOptions = {
    qos: 1,
  };
  private readonly mqttClient: MqttClient
  private readonly topic: string

  private constructor(mqttClient: MqttClient, topic: string) {
    this.mqttClient = mqttClient;
    this.topic = topic;
  }

  public static init(mqttClient: MqttClient, topic: string) {
    console.log('Starting Subscriber Basic...');

    const instance = new this(mqttClient, topic);

    instance.mqttClient.subscribe(instance.topic, instance.options);

    instance.mqttClient.on('message', (topic, payload, packet) => {
      console.log({packet});

      const payloadString = payload.toString()
      const { relayState } = JSON.parse(payloadString);

      console.log({payloadString});

      if (
        !packet.properties ||
        packet.properties?.correlationData?.toString() !== 'secret' ||
        !packet.properties?.responseTopic
      ) {
        return
      }

      const responseData = {
        error: false,
        message: `${relayState === 1 ? 'relay opened' : 'relay closed'}`,
      };
      const { responseTopic } = packet.properties;
      
      instance.mqttClient.publish(
        responseTopic,
        JSON.stringify(responseData)
      );
    });

    return instance;
  }
}

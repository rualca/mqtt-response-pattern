"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.publishWithResponseBasic = void 0;
function publishWithResponseBasic({ client, message, publishOptions, requestTopic, responseTopic, }) {
    return new Promise((resolve, reject) => {
        const relayRequestMessage = {
            relayState: message,
        };
        client.subscribe(responseTopic);
        client.once('message', (topic, payload) => {
            client.unsubscribe(responseTopic);
            try {
                const relayResponseMessage = JSON.parse(payload.toString());
                relayResponseMessage.error
                    ? reject(relayResponseMessage.message)
                    : resolve(relayResponseMessage.message);
            }
            catch (error) {
                resolve('JsonConvertError');
            }
        });
        client.publish(requestTopic, JSON.stringify(relayRequestMessage), publishOptions);
    });
}
exports.publishWithResponseBasic = publishWithResponseBasic;
//# sourceMappingURL=mqtt-async.helper.js.map